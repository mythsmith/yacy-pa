#!/usr/bin/python3
"""
Generate host:region mapping, regions.json
"""
import os
import json
import requests
import collections


if not os.path.exists('input/amministrazioni.txt'):
    r = requests.get('https://www.indicepa.gov.it/ipa-dati/dataset/502ff370-1b2c-4310-94c7-f39ceb7500e3/resource/3ed63523-ff9c-41f6-a6fe-980f3d9e501f/download/amministrazioni.txt')
    out = open('input/amministrazioni.txt','wb')
    for c in r.iter_content():
        out.write(c)
    out.close()

n = 0
hosts = {}

COL_CAP = 5
COL_PROVINCE = 6
COL_REGION = 7
COL_HOST = 8

for i, line in enumerate(open('input/amministrazioni.txt','r')):
    line = line.split('\t')
    if n==0:
        n=len(line)
    if len(line)!=n:
        print('ERROR',n,len(line))
        continue
    host = line[COL_HOST].replace('http://','').replace('https://','')
    if '@' in host:
        print('ERROR: no website, mail contact:', i, host,line)
        continue
    
    while host.endswith('/'):
        host = host[:-1]
    hosts[host] = (line[COL_REGION],line[COL_PROVINCE],line[COL_CAP])
    
json.dump(hosts, open('output/regions.json','w'))
