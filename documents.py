#!/usr/bin/python3
import os, sys
import json
import collections
import subprocess
from traceback import print_exc
import dateutil.parser as parser
import concurrent.futures
from threading import Lock
import hashlib
import codecs
import re 
from time import sleep
import shutil
import redis

redis_cli = lambda: redis.Redis(**{'host':'127.0.0.1', 'db':15})

# sudo apt install -y extract libreoffice rar zip atool p7zip-full arc arj lzip lzop nomarch unace unalz unrar
codecs.register_error("strict", codecs.ignore_errors)

skip_failed_downloads = True
lock = Lock()
documents = False
delete_on_failure = True
output_path = False
hashes = set([])

timeout_wget = 900
timeout_extract = 300
timeout_libreoffice = 30

user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Brave Chrome/76.0.3809.132 Safari/537.36'

mediadir = 'storage/' # should be mounted in local storage dir
datadir = mediadir+'yacy'
yacydir =  mediadir+'nodes/'
sr = subprocess.run 

def fix_settings(n, yacydir):
    SETFILE = yacydir+'DATA/SETTINGS/yacy.conf'
    settings = open(SETFILE, 'r').read()
    settings = settings.replace('port=8090','port={}'.format(8090+n))
    settings = settings.replace('browserPopUpTrigger=true','browserPopUpTrigger=false')
    settings = settings.replace("resource.disk.used.max.steadystate=18097152","resource.disk.used.max.steadystate=900000000")
    settings = settings.replace("javastart_Xmx=Xmx9000m","javastart_Xmx=Xmx10000m")
    open(SETFILE,'w').write(settings)

def deactivate_data_dir(n):
    yacydir = '{}nodes/{}/'.format(mediadir, n)
    print('Deactivating Yacy dir', yacydir)
    fix_settings(n, yacydir)
    sr(['bash', yacydir+'/stopYACY.sh'])
    sr(['bash', yacydir+'/killYACY.sh'])
    
def activate_data_dir(n,force=False):
    yacydir = '{}nodes/{}/'.format(mediadir, n)
    print('Activating Yacy dir', yacydir)
    rp=os.path.realpath(yacydir+'/DATA')
    Lp=datadir+str(n)
    if rp==Lp:
        print('DATA path already points to',rp)
        if not force:
            return False
    if not os.path.exists(Lp) or not os.path.exists(yacydir):
        print('Initializing new node dir', Lp)
        sr(['cp','-Plr',mediadir+'yacy',yacydir])
        shutil.copytree(mediadir+'template', Lp)
    if n==111:
       return
    print('Switching DATA to',rp, Lp)
    sr(['bash', yacydir+'/stopYACY.sh'])
    sr(['bash', yacydir+'/killYACY.sh'])
    sr(['rm',yacydir+'/DATA'])
    sr(['ln','-s', Lp, yacydir+'/DATA'])
    fix_settings(n, yacydir)
    sr(['bash', yacydir+'/startYACY.sh',str(8090+n)])
    print('Activated node',n,'... waiting...')
    sleep(100)


formats = set(['ods','odt','odp'])

cache_dir = 'cache'

def wait_free_slot(futures, batch):
    if len(futures)<batch:
        return
    while 1:
        for f in futures[:]:
            if not f.running():
                futures.remove(f)
                try:
                    f.result()
                except KeyboardInterrupt:
                    raise
                except:
                    print_exc()
                return
        print('Waiting for a free slot...')
        sleep(1)



def get_document(item):
    """Retrieve document path from cache, or download it"""
    guid = item['hash']
    base = cache_dir+'/'+guid[:2]+'/'+guid
    content = base+'/file'
    part = content+'.part'
    if not os.path.exists(base):
        os.makedirs(base, exist_ok=True)
    elif os.path.exists(base+'/failed') and skip_failed_downloads:
        # Do not retry
        return content
    rt = 0
    if os.path.exists(part):
        print('Found partial file',part,os.stat(part).st_size)
        
    if (not os.path.exists(content)) or os.stat(content).st_size==0:
        print('CACHE add',item['link'], content)
        try:
            rt = subprocess.run(['wget','-U', user_agent,'-o',base+'/wget.log','-c',
            '--no-check-certificate','--timeout',str(timeout_wget),
            '--tries','1','-O',part,item['link']]).returncode
            if rt==0:
                os.rename(part,content)
                os.remove(base+'/wget.log')
        except KeyboardInterrupt:
            raise
        except:
            print_exc()
            rt=1
    if rt>0:
        open(base+'/failed','w').write('download')
        print('Download failed!',item['link'])
        return content
    
    if os.stat(content).st_size<100:
        print('Empty file')
        open(base+'/failed','w').write('empty')
    
    return content
    


def get_latest_date(content):
    pex = content+'.extract'
    if os.path.exists(pex):
        meta = open(pex,'r').read()
        return parse_latest(meta)
    filetype = subprocess.check_output(['file',content]).decode()
    filetype = filetype.replace(', ','\n')
    latest = parse_latest(filetype)
    
    if latest:
        #print('Date provided by file',latest)
        open(pex,'w').write(filetype)
        return latest
    
    exorig = subprocess.check_output(['extract',content]).decode()
    latest = parse_latest(exorig)
    
    if latest:
        #print('Date provided by original file',latest)
        open(pex,'w').write(exorig)
        return latest
    
    if 'opendocument' in exorig:
        latest = parse_xml(content)
        if latest:
            open(pex,'w').write('date - '+str(latest))
        
    
    if 'docProps/core.xml' in exorig:
        latest = parse_xml(content,'docProps/core.xml')
        if latest: 
            open(pex,'w').write('date - '+str(latest))
            return latest
    
    # Attempt a conversion to opendocument
    ext = False
    filetype = filetype.lower()
    exorig = exorig.lower()
    if 'excel' in filetype or 'cdfv2 encrypted' in filetype:
        ext = 'ods'
    elif 'word' in filetype or 'composite document file' in filetype:
        ext = 'odt'
    elif 'powerpoint' in filetype:
        ext = 'odp'
    elif 'xl/workbook' in exorig:
        ext = 'ods'
    elif 'word/document' in exorig:
        ext = 'odt'
    
    
    
    if not ext:
        print('File format not recogized', content, latest)
        return latest 
    for ext in [ext]+list(formats-set([ext])):
        rt1 = 1
        with lock:
            subprocess.run(['pkill','-9','-f','soffice.bin'])
            sleep(0.2)
            rt1 = subprocess.run(['libreoffice','--headless','--convert-to',ext,
                                  '--outdir',os.path.dirname(content), content], 
            timeout = timeout_libreoffice).returncode
        
        output = '.'.join(content.split('.')[:-1])+'.'+ext
        
        if rt1==0 and os.path.exists(output):
            os.rename(content,content+'.orig')
            #os.rename(output, content)
            print('Converted to ', ext, content)
            break
        else:
            print('Failed conversion to',ext, rt1, content)
            
    ex = subprocess.check_output(['extract',output]).decode()
    open(pex,'w').write(ex)
    latest = parse_latest(ex)
    print('Newly extracted:', latest, output, content)
    if not latest:
        latest = parse_xml(output)
    return latest

def get_ooxml(path, item):
    # Scan the original file, not the converted ODF 
    if os.path.exists(path+'.orig'):
        path = path+'.orig'
    strict, version, app = [False]*3
    try:
        strict, version, app = parse_xml(path, 'docProps/app.xml')
    except:
        print('get_ooxml failed',path)
        return item
    if strict:
        item['category'] = 'ooxml'
    if version:
        item['AppVersion'] = version
    if app:
        item['Application'] = app
    return item
    
    

regions = json.load(open('output/regions.json','r'))
def get_region(h):
    r = regions.get(h, False)
    if r:
        return r
    # real host is a fragment of db host
    for hr in regions:
        if (hr in h):
            #print('Translated host:', h, hr)
            return regions[hr]
    # db host is a fragment of real host
    for hr in regions:
        if (h in hr):
            return regions[hr]
    print('Cannot find region',h)
    return r


def parse_generator(ex):
    generator = []
    for meta in ex.splitlines():
        meta = meta.split(' - ')
        if not 'generator' in meta[0].lower():
            continue
        return ' - '.join(meta[1:])

def parse_latest(ex):
    fail = []
    latest = False
    for meta in ex.splitlines():
        meta = meta.split(' - ')
        k = ('date','data','modified','created')
        k = [(kw in meta[0].lower()) for kw in k]
        if not sum(k):
            continue
        value = meta[-1]
        try:
            d = parser.parse(value)
            if latest is False or d>latest:
                latest = d
        except KeyboardInterrupt:
            raise
        except:
            #print('No date',value, type(value))
            fail.append(value)
            continue
    return latest

get_tag = lambda s,tag: s.split('<'+tag+'>')[1].split('</'+tag+'>')[0]

def parse_xml(path,xml='meta.xml'):
    base = os.path.dirname(path)+'/'
    if not os.path.exists(base+xml):
        rt = subprocess.run(['unzip','-un',path,xml, '-d',base], timeout=timeout_extract).returncode
        if rt>0:
            print('Cannot extract OpenDocument',path)
            return None
    meta = open(base+xml).read()
    
    # ooxml strict detection
    if xml=='docProps/app.xml':
        strict =  'xmlns="http://purl.oclc.org/ooxml/' in meta
        version = False
        app = False
        if '<AppVersion>' in meta:
            version = get_tag(meta, 'AppVersion')
        if '<Application>' in meta:
            app = get_tag(meta,'Application')
        print('Found strict, version:',strict,version,path)
        return strict, version, app
    meta = meta.replace('<','\n').replace('>',' - ')
    latest = parse_latest(meta)
    if not latest:
        print('Cannot parse',xml,path,meta)
    return latest

mimemap = json.load(open('mimetype_map.json','r'))


def get_category(path):
    #if path.endswith('.pdf'):
    #    return None
    if path.endswith('.extract'):
        return None
    mime = subprocess.check_output(['file','--mime-type',path]).decode().split(':')[1]
    if len(mime)<8:
        print('invalid mime',path,mime)
        return None
    mime = mime.replace(' ','')
    for cat, rules in mimemap.items():
        for rule in rules:
            if isinstance(rule,list):
                if path.endswith('.'+rule[1]):
                    #print('found cat',cat,path)
                    return cat
                continue
            if re.findall(rule.replace('*','.*'), mime):
                #print('found cat',cat,path)
                return cat
    return None


def get_archive_stats(path, item, sub='archive', force=True):
    items = []
    base = os.path.dirname(path)+'/'
    ar = base+sub
    if force and os.path.exists(ar):
        shutil.rmtree(ar)
    if not os.path.exists(ar) or not os.listdir(ar):
        os.makedirs(ar, exist_ok=True)
        rt = subprocess.run(['aunpack','-qq','-f','-X',ar,path],timeout=timeout_extract).returncode
        if rt!=0:
            open(base+'failed','w').write('aunpack')
            print('Failed archive extraction',path,item)
            if delete_on_failure and path.endswith('/file'):
                os.remove(path)
            return items
        print('get_archive_stats extracted ok:',ar)
    for dirpath, dirnames, filenames in os.walk(ar):
        if '__MACOSX' in dirpath:
            continue
        for f in filenames:
            if f.endswith('.extract'):
                continue
            subpath = os.path.join(dirpath,f)
            category = get_category(subpath)
            #if category=='pdf':
            #    continue
            subitem = item.copy()
            subitem['archive_path'] = subpath[len(ar):]
            if category=='archive':
                subdir = f+'.archive'
                if subdir==os.path.basename(dirpath):
                    print('Breaking recursive archive:',path,f,item['link'])
                    continue
                items += get_archive_stats(subpath,subitem,sub=subdir)
                continue
            if category:
                subitem['category']=category
                print('Scanning file in archive',category,subpath,subitem['link'])                
                subitem['ext']=f.split('.')[-1]
                subitem['hash'] = subitem['hash']+subitem['archive_path']
                subitem = add_document_stats(category, subitem, subpath)

                items.append(subitem)
            elif subpath[-3:] in ('doc', 'odt', 'ods', 'xls', 'odp', 'ppt', 'pdf'):
                print('Failed to recognize file format',category, subpath, subitem['link'])
            #elif not subpath.endswith('.pdf') and not subpath.endswith('.extract'):
            #    print('undefined category for file in archive',subpath, subitem['link'])
    if len(items):
        print('Found in archive:',len(items),path,item['link'])
    else:
        print('No docs in archive',path,item['link'])
    return items

get_hash = lambda s: hashlib.md5((s.encode())).hexdigest()

def add_document_stats(category, item, path):
    item['category'] = category
    item['size'] =0
    if os.path.exists(path):
        item['size'] = os.stat(path).st_size
    r = item.get('region',False)
    if not r:
        r = get_region(item['host'])
        if r:
            reg,prov,cap=r
            item.update({'region':reg,
             'prov':prov,
             'cap':cap})
            
    # Run extract
    latest = False
    if item['size']>100:
        if item['category']=='msofficexml':
            item = get_ooxml(path, item)
        if category=='archive':
            items = get_archive_stats(path, item)
            for sub in items:
                if not sub['latest']:
                    continue
                L = parser.parse(sub['latest']).replace(tzinfo=None)
                print('ARCHIVE DATES',latest,L)
                latest = L if ((not latest) or (L>latest)) else False
        else:
            latest = get_latest_date(path)
        if latest:
            latest = str(latest)
        else:
            print('Cannot find a valid date',item['hash'],item['size'],category,item['link'])
    else:
        print('Empty',category,path,item['link'])
        if delete_on_failure and os.path.exists(path):
            os.remove(path)
        
    item['latest'] = latest
    append_document(item)
    return item

def append_document(item):
    hashes.add(item['hash'])
    doc = json.dumps(item)
    r = redis_cli()
    o = r.hset('documents', item['hash'], doc)
    print('appended',item,o)



def grep_previous_output(itemhash):
    r = redis_cli()
    #if r.hexists('documents',itemhash):
    #    return 0
    f = r.hgetall('documents', itemhash)
    print('found',itemhash,f)
    return f



def get_document_stats(category, item, refresh=False):
    """Download a solr document and search latest modified date"""
    item['hash']= get_hash(item['link'])
    if item['hash'] in hashes:
        print('DUPLICATE',item['hash'],item['link'])
        return False
    if not refresh:
        found = grep_previous_output(item['hash'])
        if found:
            return found
    path = get_document(item)
    return add_document_stats(category, item, path)


def export_documents():
    r = redis_cli()
    i = 0
    out = open('output/documents.json.temp','wb')
    out.write(b'[')
    cursor = 0
    ar = 0
    while 1:
        cursor, keys = r.hscan('documents',cursor)
        #print('cursor',cursor)
        if not keys:
            break
        for k,doc in keys.items():
            if b'archive_path' in doc:
                print('Doc',i, k, len(doc), doc)
                ar +=1
            out.write(doc+b',\n')
            i += 1
        if cursor==0:
            break
    out.write(b'\n]')
    out.close()
    print('docs',i,'in arc',ar)


if __name__=='__main__':
    #export_documents()
    #sys.exit()
    from collections import defaultdict
    itm = defaultdict(lambda: '')
    #r= add_document_stats('archive',itm, 'cache/04/043dcd1b7c7a7c46a2cbc805716a069d/file')
    r= add_document_stats('archive',itm, 'cache/5b/5ba8a67c72108688909b933629d73f93/file')
    sys.exit()
    r = add_document_stats('msoffice',itm, 'cache/db/db3fbd389861bcdf453cd40c5c02e844/file')
    #r = add_document_stats('archive',itm, 'cache/1f/1faddf158aa1489136f9bf975ad3ff25/file')
    #r = add_document_stats('msoffice',itm, 'cache/1f/1faddf158aa1489136f9bf975ad3ff25/archive/estrazione.xls')
    print(r)
    #r = add_document_stats('msofficexml',itm,'cache/93/9364a9a688e90a61264fe32f398cf17d/file')
    #print(r)
    #r = add_document_stats('pdf',defaultdict(lambda: ''),'cache/62/62dc4e946a7f23ca5e57e78d28b656a0/file')
    #r = add_document_stats('pdf',defaultdict(lambda: ''),'cache/85/85da5ce038290740998f0288af1aec09/file')
    #print(r)
