#!/usr/bin/python3
import os, sys
import json
import requests
import documents 
import concurrent.futures
from traceback import print_exc 
p=os.path.abspath('.')
if p not in sys.path:
    sys.path.insert(0, p)

import documents

batch_size = 150

executor = concurrent.futures.ThreadPoolExecutor()

documents.cache_dir = 'cache'
documents.fopen('output/cc/documents.json')

if not os.path.exists('input/comuni-crawler.json'):
    r = requests.get('https://gitlab.com/amreo/comuni-crawler/-/raw/master/output_collection/2021-05-06.json')
    out = open('input/comuni-crawler.json','wb')
    for c in r.iter_content():
        out.write(c)
    out.close()

catmap = {'msoffice':['doc','xls','ppt'],
          'msofficexml':['docx', 'xlsx','pptx'],
          'opendocument':['odt','ods','odp']}


futures = []
for i, line in enumerate(open('input/comuni-crawler.json','r')):
    line = json.loads(line)
    scanning = 0
    
    for category, extensions in catmap.items():
        for ext in extensions:
            for link in line['documenti'][ext+'_lista']:
                
                item = {'host':line['sito_web_indicato'],
                        'link':link}
                documents.wait_free_slot(futures,batch_size)
                f = executor.submit(documents.get_document_stats, category, item)
                futures.append(f)
                scanning+=1
    print('scanning',i, scanning,line['sito_web_indicato'])
    

concurrent.futures.wait(futures)
documents.fclose()
