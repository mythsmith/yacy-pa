 #!/bin/bash
DONE=(".num." ".num.")
BASE=/data/yacy/yacy-pa
ACT=$1
if [ "$ACT" == "" ]; then
    ACT="start"
fi

if [ "$ACT" != "start" ]; then
    if [ "$ACT" != "stop" ]; then
        echo "Either specify start/stop as first arg"
        exit 1
    fi
fi


START=$2
if [ "$START" == "" ]; then
   START=0
fi 

END=$3
if [ "$END" == "" ]; then
   END=$START
fi 


echo "CLUSTER: $ACT from $START to $END"
  
 for i in $(seq $START $END);
    do  
        echo "CLUSTER $ACT: $i (from $START to $END)"
        pkill -9 -f "crawl.py $i"
        tmux kill-session -t yacy$i
        cd $BASE/storage/nodes/$i
        ./stopYACY.sh
        ./killYACY.sh
        if [ "$ACT" == "stop" ]; then
            echo "done stopping $i";
            continue
        fi
	if [[ " ${DONE[*]} " =~ " .${i}. " ]]; then
                echo "Skip done: $i"
                continue
        fi
        cd $BASE
        echo "Starting in tmux yacy$i"
        tmux new-session -d -s yacy$i ./crawl.py $i
        sleep 3
 done

