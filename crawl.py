#!/usr/bin/python3
import os
import sys
import fcntl
import requests
import subprocess
from time import sleep, time
import random
from requests.auth import HTTPDigestAuth
from traceback import print_exc
from datetime import datetime
from collections import defaultdict
import documents
from documents import redis_cli
import json
import redis

ACTIVE_SLEEP = 60
ACTIVE_NODE = None
current_inode = None
argv = sys.argv[:]+['0']
try:
    current_inode = int(argv[1])
    ACTIVE_NODE = '127.0.0.1:{}'.format(8090+current_inode)
except:
    pass
print('ACTIVE_NODE', argv, current_inode, ACTIVE_NODE)


lockfile = open('lock','a')
last_restart = datetime.now()
batch_size = 150
crawl_timeout = 3600*24*3 # 3 days
Y_USER, Y_PASSWORD = open('cred.txt','r').read().split('\n')[:2]
auth = HTTPDigestAuth(Y_USER,Y_PASSWORD)

totals = {'paused':[], 'ppm':0,'db':0,'hosts':[]}
pending = {} #host:handle
max_hosts_per_inode = 2000
max_db_per_inode = 2e6


CALL="http://{}/Crawler_p.html"
params={'crawlingDomMaxPages': '10000', 'crawlingQ': 'on', 'crawlingMode': 'url', 'range': 'domain', 'directDocByURL': 'on', 
        'crawlingstart': '', 'xsstopw': 'on', 'indexMedia': 'on', 'cachePolicy': 'iffresh', 'indexText': 'on', 'crawlingDepth': '4', 
        'collection': 'amministrazioni', 'crawlerAlwaysCheckMediaType': 'true', 'recrawl': 'nodoubles','agentName':'Random Browser'}


PENDING= "http://{}/api/status_p.xml"

RESTART="http://{}/Steering.html"

HOSTS = "http://{}/IndexBrowser_p.html?hosts"

terminate = {'terminate':'Terminate','handle':''}





def set_done(url, t):
    redis_cli().hset(b'done_'+url,mapping={'started':t,'node':current_inode})
    redis_cli().sadd('node_{}'.format(current_inode),url)
    

def terminate_crawl(handle, node=None):
    node = node or ACTIVE_NODE
    terminate['handle']=handle
    r = requests.get(CALL.format(node), auth=auth, params=terminate, timeout=60)
    print('Terminating crawl',node, r.status_code, url)
    print(r.content)

def submit_url(url, node=None):
    node = node or ACTIVE_NODE
    print('submit',node, url)
    params['crawlingURL']=url
    r = requests.get(CALL.format(node), auth=auth, params=params, timeout=60)
    print(r)
    if r.status_code!=200:
        print('ERROR', r.content)
        return False
    print('start a new crawl:',url,r)
    set_done(url, time())
    return True

def get_pending(node=None):
    node = node or ACTIVE_NODE
    global totals, pending
    r = requests.get(PENDING.format(node), auth=auth, timeout=60)
    if r.status_code!=200:
        print(r.content)
        print('ERROR')
        return 1e3, 0, 0, 0
    c = r.content.decode()
    n = c.split('<crawls count="')[1].split('"')[0]
    ppm = c.split('<ppm>')[1].split('</ppm>')[0]
    db = c.split('<urlpublictext>')[1].split('</urlpublictext>')[0]
    crawl = 'paused' in c.split('<localcrawlerqueue>')[1].split('</localcrawlerqueue>')[0]
    print('Pending:',node, n, 'paused:',crawl, 'ppm:',ppm,'db:',db)
    # Remove pending in this node
    pending = {}
    rcli = redis_cli()
    # Add currently pending
    for h in c.split('<name>')[1:]:
        host,hid = h.split('</name>')
        hid = hid.split('<handle>')[1]
        hid = hid.split('</handle>')[0]
        pending[host] = hid
        rcli.sadd('pending_{}'.format(current_inode), "{}$@${}".format(hid,host))
    #TODO: parse all current crawls, so they can be stopped if too old
    
    ndone = len(rcli.keys('done_*'))
    pc = 100.*ndone/(ndone+redis_cli().scard('urls'))
    totals = {'ppm':int(ppm),'db':int(db),'paused': int(crawl),'pending':int(n)}
    print('{:.2f}%'.format(pc),'PPM:',totals['ppm'],'DB:',totals['db'],'Hosts:',ndone,'rem',rcli.scard('urls'))
    rcli.hset('pending_tot_{}'.format(current_inode), mapping=totals)
    totals['hosts'] = pending.copy()
    return int(n), crawl, ppm, db
    





def check_restart():
    global last_restart
    # Restart every 12h
    if (datetime.now()-last_restart).total_seconds()<3*86400:
        return False
    requests.get(RESTART.format(ACTIVE_NODE),auth=auth,params={'restart':''}, timeout=60)
    last_restart = datetime.now()
    return True


def get_hosts(node=None):
    node = node or ACTIVE_NODE
    r = requests.get(HOSTS.format(node), auth=auth,params={"hosts":''}, timeout=60)
    hosts = []
    r = r.content
    for v in r.split(b"IndexBrowser_p.html?path="):
        if b'&facetcount=' not in v:
            continue
        h = v.split(b'&facetcount=')[0]
        hosts.append(h.decode())
    print('Node hosts:',node, current_inode, len(hosts))
    return hosts

def check_done_hosts(url):
    rcli = redis_cli()
    for host in rcli.keys('done_*'):
        if url in host:
            vals = rcli.hgetall(host)
            print('URL already crawled in ', host, vals)
            return int(vals[b'node'])
    return False



def check_stale_crawls(pending):
    t0 = time()
    s = 0
    ages = []
    rcli = redis_cli()
    for url,handle in pending.items():
        for pre in ('http','https','ftp'):
            url1 = pre+'://'+url
            if rcli.exists('done_'+url1):
                break
        if not rcli.exists('done_'+url1):
            #print('Not found in done: terminating',url)
            continue
        u = rcli.hgetall('done_'+url1)
        t = float(u[b'started'])
        if t==0:
            continue
        ages.append((t0-t)/3600)
        if t0-t>crawl_timeout:
            print('Crawl timeout', handle, url, url1)
            terminate_crawl(handle)
            s+=1
    a = len(ages)
    print('Stopped stale crawls: {}; total: {}/{}, mean age: {:.1f}; max: {:.1f}'.format(s,a,len(pending), sum(ages)/(a or 1), a==0 or max(ages)))
    
nodemax = -1
for d in os.listdir(documents.mediadir):
    if not d.startswith('yacy'):
        continue
    if d=='yacy-pa':
        continue
    if not len(d)>4:
        continue
    i = int(d[4:])
    if i>nodemax:
        nodemax=i

if nodemax<0: nodemax=0

def clear_hosts():
    rcli = redis_cli()
    for i in range(0,nodemax+1):
        rcli.delete('node_{}'.format(i))
    for k in rcli.keys('done_*'):
        rcli.delete(k)

def calc_hosts():
    global totals, pending, current_inode
    rcli = redis_cli()
    start = current_inode
    for current_inode in range(start,nodemax+1):
        documents.activate_data_dir(current_inode,force=True)
        addr = '127.0.0.1:{}'.format(8090+current_inode)
        hl = get_hosts(addr)
        rcli.sadd('node_{}'.format(current_inode),*hl)
        for url in hl:
            k = 'done_'+url
            if not rcli.exists(k):
                rcli.hset(k,mapping={'node':current_inode,'started':0})
        get_pending(addr)
        documents.deactivate_data_dir(current_inode)


if 'clear' in sys.argv:
    current_inode = 0
    clear_hosts()
    calc_hosts()
else:
    map_hosts.update(json.load(open('map_hosts.json','r')))
    if os.path.exists('pending_hosts.json'):
        pending_hosts.update(json.load(open('pending_hosts.json','r')))


if 'check' in sys.argv:        
    calc_hosts()
    sys.exit()
    

    


rcli = redis_cli()

donekeys = [k.decode() for k in rcli.keys('done_*')]
urls = list(set(open('url_amm.csv','r').read().split('\n'))-set(donekeys))
rcli.sadd('urls',*urls)


for url in donekeys:
    m = rcli.hgetall(url)
    n = int(m[b'node'].decode())
    if n==current_inode:
        rcli.sadd('node_{}'.format(n),url)



for n in rcli.keys('node_*'):
    print('Loaded ',n,rcli.scard(n))

documents.activate_data_dir(current_inode, force=True)
last_restart = datetime.now()

while 1:
    try:
        get_pending()
        break
    except:
        print('Cannot get_pending!')
        print_exc()
        sleep(60)
        continue

def next_url(rcli):
    url = rcli.spop('urls')
    if not url:
        print('FINISHED!')
        return None
    print('Next url:',url) 
        
    node = check_done_hosts(url)
    if node:
        set_done(url, 0)
        return False
    return url


while 1:
    rcli = redis_cli()
    try:
        check_restart()
        queue = totals['pending']
        db =int(rcli.hget('pending_tot_{}'.format(current_inode), 'db')) 
        hn = rcli.scard('node_{}'.format(current_inode))
        
        if db>=max_db_per_inode:
            if queue:
                print("Max db size reached {} with hosts {} on node {}, waiting for pending to finish: {}".format(db,hn,current_inode, queue))
                sleep(ACTIVE_SLEEP)
                get_pending()
                continue
            print('Node completed.', current_inode)
            break
        if get_pending()[0]>=batch_size:
            print('Waiting for a free slot on inode:', current_inode)
            sleep(ACTIVE_SLEEP)
            continue
        url = next_url(rcli)
        if url is None:
            break
        if not url:
            continue
        sub = submit_url(url)
        check_stale_crawls(pending)
        if sub:
            break
        else:
            sleep(ACTIVE_SLEEP)
    except:
        print_exc()
        sleep(10)

