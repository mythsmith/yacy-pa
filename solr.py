#!/usr/bin/python3
import os, sys
import json
import requests
import collections
import subprocess
from traceback import print_exc, format_exc
import dateutil.parser as parser
import concurrent.futures
import hashlib
from time import sleep
import shutil
from requests.auth import HTTPDigestAuth
import subprocess

p=os.path.abspath('.')
if p not in sys.path:
    sys.path.insert(0, p)

import documents

Y_USER, Y_PASSWORD = open('cred.txt','r').read().split('\n')[:2]
auth = HTTPDigestAuth(Y_USER,Y_PASSWORD)

batch_size = 500
scan_size = 8


executor = concurrent.futures.ThreadPoolExecutor()

mimap = json.load(open('mimetype_map.json','r'))
base_url = "http://{node}/solr/collection1/select?q={field}:{query}&defType=edismax&start={start}&rows={rows}&facet=true&facet.mincount=1&facet.field={field}&wt=yjson"

futures = []

PAUSE = "http://{}/Status.html?pauseCrawlJob=&jobType=localCrawl"
RESUME = "http://{}/Status.html?continueCrawlJob=&jobType=localCrawl"

queries_dir = 'output/queries/'

def control(node, pause=True):
    url = (RESUME,PAUSE)[pause]
    u = url.format(node)
    r = requests.get(u, timeout=30, auth=auth)
    print(u, r)
    

def save_batch(category, query_url, node, start, **query):
    """Retrieve all query results and scan each response"""
    r = requests.get(query_url, timeout=60, auth=auth)
    items = json.loads(r.content)['channels'][0]
    items = items['items']
    if not items:
        print('No more items', category)
        return 0
    n = int(node.split(':')[-1])-8090
    out = queries_dir + '{}_{}_{}'.format(category, n, start)
    for k,v in query.items():
        v = v.replace('/',':').replace('*','...')
        out ='{}_{}={}'.format(out, k, v)
    json.dump(items, open(out+'.json','w'))
    #if category=='pdf':
    #    for item in items:
    #        documents.append_document(item)
    #    return len(items)
    return len(items)
    
def scan_batch(items, category, node='unknown', refresh=False):
    global futures
    for item in items:
        documents.wait_free_slot(futures, scan_size)
        item['node'] = node
        f = executor.submit(documents.get_document_stats, category, item, refresh=refresh)
        futures.append(f)
    print('Batch done', category, len(items))
    return len(items)


def scan_queries(categories=[], refresh=False):
    global futures
    os.makedirs(queries_dir+'/done', exist_ok=True)
    for f in os.listdir(queries_dir):
        if not f.endswith('.json'):
            continue
            
        category, node = f.split('_')[:2]
        if categories and category not in categories:
            continue

        f = os.path.join(queries_dir,f)
        print('SCANNING ',category,node,f)
        items = json.load(open(f,'r'))
        scan_batch(items, category, node, refresh=refresh)
        os.rename(f,f'{queries_dir}/done/{os.path.basename(f)}')
    documents.wait_free_slot(futures, 1)  
    print('SCAN ended')

def scan_query(node, category, **query):
    start = 0
    ret = 1
    tot = 0
    while ret:
        query_url = base_url.format(node=node, start=start, rows=batch_size, **query)
        try:
            ret = save_batch(category, query_url, node, start, **query)
        except KeyboardInterrupt:
            raise
        except:
            print('ERROR processing batch',query_url)
            print_exc()
            ret = 0
        tot += ret
        start = start+batch_size
    print('DONE ',node, query, tot)
    return tot



def scan_map(node):
    for category, queries in mimap.items():
        if category=='pdf':
            continue
        for query in queries:
            if shutil.disk_usage('./cache').free<1e9:
                raise BaseException('No more disk space!')
            field='content_type'
            if isinstance(query,list):
                field, query = query
            r = scan_query(node, category, field=field, query=query)

def run_queries():
    try:
        for n in range(0,31):
            documents.activate_data_dir(n, force=True)
            sleep(300)
            node = '127.0.0.1:{}'.format(8090+n)
            control(node, pause=True)
            scan_map(node)
            control(node, pause=False)
            documents.deactivate_data_dir(n)
    except:
        print('scan_map error:')
        print_exc()

if __name__=='__main__':
    if 'scan' in sys.argv:
        #scan_queries()
        scan_queries(['archive'], True)
        documents.export_documents()
    else:
        run_queries()
        
