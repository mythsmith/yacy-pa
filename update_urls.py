#!/usr/bin/python3
from urllib.request import urlopen
import json
url = 'https://indicepa.gov.it/ipa-dati/api/3/action/datastore_search?resource_id=d09adf99-dc10-4349-8c53-27b1e5aa97b6&limit=100000&q=Pubbliche%20Amministrazioni'
dat = urlopen(url)
dat = dat.read()
d = json.loads(dat)['result']['records']
out = []
n = len(d)
urlist = open('url_amm.csv','w')
while len(d):
    e = d.pop(0)
    if e['Tipologia'] == 'Pubbliche Amministrazioni' and e['Sito_istituzionale']:
        out.append(e)
        s = e['Sito_istituzionale'].lower()+'\n'
        if not s.startswith('http'):
            s = 'http://'+s
        urlist.write(s)
    else:
        print('Skip', e['Denominazione_ente'], e['Sito_istituzionale'], e['Tipologia'])
        

print('Found',len(out), 'of', n, out[0])
json.dump(out, open('urls.json','w'))
