#!/bin/bash
pkill -9 -f clamscan
for f in cache/*; do
    echo $f
    tmux new-session -d -s $f "clamscan --bell -i -l $f/clamscan.log -r $f "
    while [ `ps aux|grep clamscan|grep cache|wc --lines` -gt 20 ]; do
        echo Sleeping
        sleep 60
        done
done
